<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class NeoController extends Controller
{
/**
 * Get Desired data from Api
 * This function to fetch Asteroid data  
 * By hitting Api With Get Method And Get Response In Json.
 * Made An Array With Respective Date And Neo Count
 * @param  \Illuminate\Http\Request  $request
 * @return *graph details
 */
    public function getstats(Request $request){
      
        $response = Http::get('https://api.nasa.gov/neo/rest/v1/feed?start_date='.$request->start_date.'&end_date='.$request->end_date.'&detailed=true&api_key=DEMO_KEY');
        foreach($response['near_earth_objects'] as  $key => $data){
            $full['date'] = $key; 
            $full['count'] = count($data);
            $details[] = $full;
        }
        return $details;
    }
}
